﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParticleSwarmOptimization
{
    public interface IFunction
    {
        int Dimension { get; }

        double Value(double[] x);

        IFunction CreateCopy();

        double[] GetContinuousRepresentation();

        double[] GetRandomSolution();

        double CreateInitialSolution();
        double CreateHistoricSolution();

        double InitializationRadius { get; }

        void AdaptFunctionStates(List<IFunction> function);
    }
}
