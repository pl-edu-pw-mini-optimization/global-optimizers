﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace ParticleSwarmOptimization
{
    public class Utils
    {
        private Random _random;

        [ThreadStatic]
        private static Utils _instance;

        public static Utils Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Utils();
                }
                return _instance;
            }
        }

        private Utils()
        {
            _seed = (int)(DateTime.Now.ToFileTimeUtc() % int.MaxValue);
            _random = new Random(_seed);
        }

        public Random Random
        {
            get
            {
                return _random;
            }
        }


        private static readonly object locklog = new object();
        private int _seed;

        public int Seed
        {
            get
            {
                return _seed;
            }
        }

        public void ResetRandom(int seed)
        {
            _seed = seed;
            _random = new Random(_seed);
        }

        public double[] GetRandomInSphere(int n, double radius = 1.0)
        {
            double[] x = new double[n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < 100; j++)
                {
                    x[i] += Random.NextDouble() - 0.5;
                }
                x[i] /= 100;
            }
            double randomRadius = Random.NextDouble() * radius;//(new ContinuousUniform(0.0, radius)).Sample();
            double normalFactor = EuclideanDistance(x, new double[n]);
            for (int i = 0; i < n; ++i)
                x[i] *= randomRadius / normalFactor;
            return x;
        }

        public double EuclideanDistance(double[] x, double[] y)
        {
            double dist = 0.0;
            for (int i = 0; i < x.Length && i < y.Length; ++i)
                dist += (x[i] - y[i]) * (x[i] - y[i]);
            return Math.Sqrt(dist);
        }


        public double EuclideanDistance(int[] x, int[] y)
        {
            double dist = 0.0;
            for (int i = 0; i < x.Length && i < y.Length; ++i)
                dist += (x[i] - y[i]) * (x[i] - y[i]);
            return Math.Sqrt(dist);
        }

    }
}
