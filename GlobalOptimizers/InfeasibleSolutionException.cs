﻿using System;

namespace ParticleSwarmOptimization
{
    public class InfeasibleSolutionException : ArgumentException
    {
        public InfeasibleSolutionException(string message) : base(message)
        {
        }
    }
}
