﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ParticleSwarmOptimization
{
    public class PSOAlgorithm
    {
        private Particle[] particles;

        public Particle[] Particles
        {
            get { return particles; }
        }

        public double SwarmDiameter
        {
            get { return particles.Where(particle => particle != null).Max(particle => particles.Max(particle2 => Utils.Instance.EuclideanDistance(particle2.x, particle.x))); }
        }

        public double WorstValue
        {
            get { return particles.Where(particle => particle != null).Max(particle => particle.bestValue); }
        }

        public double BestValue
        {
            get; protected set;
        }

        public double[] Best
        {
            get; protected set;
        }

        public double AvarageSpeedValue
        {
            get
            {
                return particles.Where(particle => particle != null).Average(particle => Utils.Instance.EuclideanDistance(particle.v, new double[particle.v.Length]));
            }
        }

        public int Iterations = 0;
        private IFunction function;
        private double radius;
        private double inventorsRatio;

        public PSOAlgorithm(IFunction function, int swarmSize, double radius, double neighbourhood, double inventorsRatio)
        {
            ZeroGlobalBest(function);
            this.function = function;
            this.inventorsRatio = inventorsRatio;
            this.radius = radius;
            particles = new Particle[swarmSize];
            InitializeSwarm(function, swarmSize, inventorsRatio);
            for (int i = 0; i < swarmSize; ++i)
            {
                List<int> randomToss = new List<int>();
                List<int> urn = new List<int>();
                for (int a = 0; a < swarmSize; ++a)
                {
                    urn.Add(a);
                }
                while (randomToss.Count < Math.Min(40, swarmSize))
                {
                    int choice = Utils.Instance.Random.Next(urn.Count);
                    randomToss.Add(urn[choice]);
                    urn.RemoveAt(choice);
                }
                for (int j = 0; j < randomToss.Count; j++)
                {
                    if ((particles[i] is Inventor || Utils.Instance.Random.NextDouble() < neighbourhood) && i != j)
                        particles[i].neighbours.Add(particles[randomToss[j]]);
                }
                particles[i].InitializeVelocity();
            }

        }

        private void ZeroGlobalBest(IFunction function)
        {
            this.BestValue = double.PositiveInfinity;
            this.Best = new double[function.Dimension];
        }

        private void InitializeSwarm(IFunction statefullQualityFunction, int swarmSize, double inventorsRatio)
        {
            //Utils.random = new Random((int)(DateTime.Now.Ticks % int.MaxValue));
            //tworzymy kopie, bo obiekty funkcji mogą byc stanowe
            var historicalQualityFunction = statefullQualityFunction.CreateCopy();
            statefullQualityFunction.CreateInitialSolution();
            historicalQualityFunction.CreateHistoricSolution();
            statefullQualityFunction.AdaptFunctionStates(new List<IFunction>()
            {
                statefullQualityFunction,
                historicalQualityFunction
            });
            var initSolution = statefullQualityFunction.GetContinuousRepresentation();
            var histSolution = historicalQualityFunction.GetContinuousRepresentation();
            do
                for (int i = 0; i < swarmSize; ++i)
                {
                    if (Utils.Instance.Random.NextDouble() >= inventorsRatio)
                        particles[i] = new Particle(statefullQualityFunction.CreateCopy());
                    else
                        particles[i] = new Inventor(statefullQualityFunction.CreateCopy());
                    particles[i].neighbours.Clear();
                    if (i == 0)
                        particles[i].InitializePosition(initSolution, 1e-300);
                    else if (i == swarmSize / 2)
                        particles[i].InitializePosition(histSolution, 1e-300);
                    else if (i % 10 == 0)
                        particles[i].InitializePosition(statefullQualityFunction.GetRandomSolution(), 1e-300);
                    else
                        particles[i].InitializePosition(initSolution, statefullQualityFunction.InitializationRadius);
                }
            while (SwarmDiameter == 0 && statefullQualityFunction.Dimension > 1 && radius > 0.0 && swarmSize > 1);
        }

        public void ReloadState(IFunction newStateFunction)
        {
            int particleToReplace = GetWorstAndResetStateToPersonalBest();
            ZeroGlobalBest(function);

            newStateFunction.CreateInitialSolution();
            var initialPositions = new List<double[]>();
            var newFunctions = new List<IFunction>()
                {
                    newStateFunction
                };
            newStateFunction.AdaptFunctionStates(
                (newFunctions.Concat(particles.Select(p => p.function))).ToList()
                )
                ;
            initialPositions = particles.Select(p => p.function.GetContinuousRepresentation()).ToList();
            var newStateInitialFunction = newStateFunction.GetContinuousRepresentation();
            this.function = newStateFunction.CreateCopy();
            for (int i = 0; i < particles.Length; ++i)
            {
                particles[i].bestValue = Double.PositiveInfinity;
                particles[i].function = newStateFunction.CreateCopy();
                if (i == particleToReplace)
                {
                    particles[i].InitializePosition(
                        newStateInitialFunction,
                        1e-300);
                }
                else
                {
                    particles[i].InitializePosition(
                        initialPositions[i],
                        1e-300);
                }
                UpdateGlobalBest(i);
            }
            for (int i = 0; i < particles.Length; ++i)
            {
                particles[i].InitializeVelocity();
            }
        }

        private void UpdateGlobalBest(int i)
        {
            if (particles[i].bestValue < BestValue)
            {
                BestValue = particles[i].bestValue;
                Best = particles[i].best.ToArray();
            }
        }

        private int GetWorstAndResetStateToPersonalBest()
        {
            var worstValue = double.MinValue;
            var particleToReplace = -1;
            for (int i = 0; i < particles.Length; ++i)
            {
                if (particles[i].bestValue > worstValue)
                {
                    particleToReplace = i;
                    worstValue = particles[i].bestValue;
                }
                particles[i].function.Value(particles[i].best);
            }
            return particleToReplace;
        }

        public void Step()
        {
            ++Iterations;
            for (int i = 0; i < particles.Length; i += 1)
            {
                particles[i].Move();
                if (particles[i].x.Any(xx => double.IsInfinity(xx) || decimal.ToDouble(decimal.MaxValue) < xx || decimal.ToDouble(decimal.MinValue) > xx))
                {
                    particles[i].InitializePosition(this.Best, radius);
                    particles[i].InitializeVelocity();
                }
                particles[i].UpdateBest();
                UpdateGlobalBest(i);
            }
            for (int i = 0; i < particles.Length; i += 1)
                particles[i].UpdateVelocity();
            if ((WorstValue - BestValue) <= 1e-6 || (Iterations % 50 == 0 && SwarmDiameter <1e-6 && AvarageSpeedValue <1e-3))
            {
                this.radius *= 1.1;
                InitializeSwarm(function, particles.Length, inventorsRatio);
            }
        }

    }
}
